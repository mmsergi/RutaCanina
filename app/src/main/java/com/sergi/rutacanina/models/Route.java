package com.sergi.rutacanina.models;

public class Route {

    private int timestamp;
    private String name;
    private String description;
    private String UID;

    private float initialLatitude;
    private float finalLatitude;
    private float initialLongitude;
    private float finalLongitude;

    private String country;

    public Route(){

    }
}
